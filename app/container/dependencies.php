<?php
#сборка зависимостей

$files = array_merge(
    glob(__DIR__ . "/config/*.php" ?: []),
    glob(__DIR__ . "/config/dependencies/*.php" ?: []),
    glob(__DIR__ . "/../{$clientConfigFolder}/config/*.php" ?: [])
);

$config = array_map(function ($file) {
    return require $file;
}, $files);

return array_merge_deep($config);

#на 7.1 нет рекурсивного мержа, пришлось копипастнуть
function array_merge_deep($arrays) {
    $result = array();

    foreach ($arrays as $array) {
        if (is_array($array)) 
            foreach ($array as $key => $value) {
                // Renumber integer keys as array_merge_recursive() does. Note that PHP
                // automatically converts array keys that are integer strings (e.g., '1')
                // to integers.
                if (is_integer($key)) {
                    $result[] = $value;
                }
                // Recurse when both values are arrays.
                elseif (isset($result[$key]) && is_array($result[$key]) && is_array($value)) {
                    $result[$key] = array_merge_deep(array($result[$key], $value));
                }
                // Otherwise, use the latter value, overriding any previous value.
                else {
                    $result[$key] = $value;
                }
        }
    }

    return $result;
}