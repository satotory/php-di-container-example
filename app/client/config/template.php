<?php

return [
    "Class2001" => DI\Factory(function () {
        return new App\Classes\M2001\Seporated\client\Class2001(require $_SERVER['DOCUMENT_ROOT'] . "/../container/config/M2001/RecordTemplate.php" ?: []);
    }),
    "RecordTemplate" => function (\Psr\Container\ContainerInterface $c) {
        return new App\Classes\M2001\Seporated\client\RecordTemplate($c->get('Class2001'));
    }
];