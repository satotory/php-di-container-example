<?php

namespace App\Classes\M2001\Interfaces;

use App\Classes\M2001\Interfaces\IRecordTemplate;

interface IClass2001
{
    public function __construct(array $data);

    public function getRecordTemplate(IRecordTemplate $template);

    public function __get($key);
}