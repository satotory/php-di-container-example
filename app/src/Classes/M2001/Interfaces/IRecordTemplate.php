<?php

namespace App\Classes\M2001\Interfaces;

use App\Classes\M2001\Interfaces\IClass2001;

interface IRecordTemplate
{
    public function __construct(IClass2001 $object);

    public function getTemplate();
}