<?php

namespace App\Classes\M2001\Seporated\client;

use App\Classes\M2001\Interfaces\IRecordTemplate;
use App\Classes\M2001\Interfaces\IClass2001;

class Class2001 implements IClass2001
{
    public $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getRecordTemplate(IRecordTemplate $template)
    {
        return $template->getTemplate();
    }

    public function getStock()
    {
        return $this->stock - 5;
    }

    public function __get($key)
    {
        return $this->data[$key];
    }
}