<?php

namespace App\Classes\M2001;

use App\Classes\M2001\Interfaces\IRecordTemplate;
use App\Classes\M2001\Interfaces\IClass2001;

class RecordTemplate implements IRecordTemplate
{
    public $object;

    public function __construct(IClass2001 $object)
    {
        $this->object = $object;
    }

    public function getTemplate()
    {
        $stock = $this->object->stock;
        $price = $this->object->price;

        $html = "Остаток: {$stock}, Цена: {$price}";
        
        return $html;
    }

    public function __get($key)
    {
        return $this->object->$key;
    }
}